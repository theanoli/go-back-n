/*
timestamp the event happens, the sequence number, the acknowledge, and the
timeout/retransmission events.
*/


#include <stdio.h>	
#include <stdlib.h>	
#include <sys/socket.h>	
#include <sys/types.h> 
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>	// for sockaddr_in and inet_ntoa()
#include <string.h>	
#include <unistd.h>	
#include <errno.h>
#include <memory.h>
#include <signal.h>  // for sigaction
#include <sys/time.h>


#define PAYLOAD_SIZE 500
#define HEADER_SIZE 3 * sizeof (int)
#define FNAME_BUFSIZE 128

#define DGRAM_SIZE HEADER_SIZE + PAYLOAD_SIZE 

#define TIMEOUT_SECS 3	/* Seconds between retransmits */

#define LOSS_RATE 0.1
#define CORRUPT_RATE 0.1

#define WND_SIZE PAYLOAD_SIZE * 5

typedef struct gbnpacket {
	int type;
	int seq_no;
	int length;
	char data[PAYLOAD_SIZE];
} gbnpacket_t;

void error (char *msg)
{
	perror(msg);
	exit(0);
}

size_t type_offset = 0;
size_t seq_no_offset = sizeof (int);
size_t length_offset = 2 * sizeof (int);
size_t data_offset = HEADER_SIZE; 

gbnpacket_t *
allocate_packet ()
{
	gbnpacket_t *packet_ptr = malloc (sizeof (gbnpacket_t)); 
	if ( packet_ptr == NULL )
		error ("Couldn't allocate space for packet!");
	memset (packet_ptr, 0, sizeof (gbnpacket_t)); 

	return packet_ptr;
}

gbnpacket_t *
make_packet (int seq_number, char *data, int type, int length)
{	
	gbnpacket_t *packet = allocate_packet ();

	packet->seq_no = seq_number; 
	packet->type = type; 
	packet->length = length; 
	if ( data == NULL )
		memset (&packet->data, 0, PAYLOAD_SIZE);
	else
		memcpy (&packet->data, data, PAYLOAD_SIZE); 

	return packet; 
}

void
send_packet (int socket, struct sockaddr *address, gbnpacket_t *packet)
{
	int n; 
	char *buffer; 

	if ( (buffer = malloc (DGRAM_SIZE)) == NULL)
		error ("Error allocating dgram buffer!");
	memset (buffer, 0, DGRAM_SIZE);

	void *seq_no_ptr = buffer + seq_no_offset; 
	void *length_ptr = buffer + length_offset; 
	void *type_ptr = buffer + type_offset; 
	void *data_ptr = buffer + data_offset;

	memcpy (seq_no_ptr, &packet->seq_no, sizeof (int));
	memcpy (length_ptr, &packet->length, sizeof (int));
	memcpy (type_ptr, &packet->type, sizeof (int));
	memcpy (data_ptr, &packet->data, packet->length);

	n = sendto (socket, buffer, DGRAM_SIZE, 0, address, sizeof (struct sockaddr_in));
	if ( n < 0 )
		error ("Error sending packet!");

	// printf ("Sent packet %d\n", packet->seq_no);


	free (buffer);
}

gbnpacket_t *
receive_packet (char *buffer) 
{
	gbnpacket_t *packet = allocate_packet (); 

	void *seq_no_ptr = buffer + seq_no_offset; 
	void *length_ptr = buffer + length_offset; 
	void *type_ptr = buffer + type_offset; 
	void *data_ptr = buffer + data_offset;	

	memcpy (&packet->seq_no, seq_no_ptr, sizeof (int));
	memcpy (&packet->length, length_ptr, sizeof (int));
	memcpy (&packet->type, type_ptr, sizeof (int));
	memcpy (&packet->data, data_ptr, packet->length);

	return packet; 
}

void 
session_lossrate (char *loss_arg, float lossrate, float corruptrate, float *loss_rate, float *corrupt_rate)
{
	*loss_rate = (atoi(loss_arg) ? lossrate : 0);	
	*corrupt_rate = (atoi(loss_arg) ? corruptrate : 0);	
	printf ("Loss rate: %f, corruption rate: %f\n", *loss_rate, *corrupt_rate); 
}
