// Arguments: sender_hostname sender_portnum filename enable_congestionControl

#include "headerGBN.h"

// Global variables
int base = 0;  // base is 0 (will be increased as wnd moves)
int sendflag, timeout;

// Function declarations
void CatchAlarm (int ignored); /* Handler for SIGALRM */
int get_filelen (FILE *, int *);
gbnpacket_t *filechunk_to_packet (FILE *, int *);
int seq_index (int); 
void dealloc_pkts (gbnpacket_t **, int);


int main (int argc, char *argv[]) {

	struct sigaction myAction;  // For timer 
 
	int nextseqidx = 0;
	int nextseqnum = 0; 
	int base = 0; 
	int seq_counter = 0;  // For incrementing sequence numbers during file reading
	int last_ack_received = -1; 

	int sockfd, portno, recvlen, datasize, total_packets, i;
	float loss_rate, corrupt_rate; 

	socklen_t clilen; 
	struct sockaddr_in my_addr;
	struct sockaddr_in rem_addr;
	socklen_t addr_len = sizeof(my_addr);

	char *fname_buffer;
	char *recv_buffer;	
	FILE *fp;


	if ( (fname_buffer = malloc (FNAME_BUFSIZE)) == NULL )
		error ("Error allocating filename buffer!");
	memset(fname_buffer, 0, FNAME_BUFSIZE);

	if ( (recv_buffer = malloc (DGRAM_SIZE)) == NULL )
		error ("Couldn't allocate sender's receive buffer!"); 
	memset (recv_buffer, 0, DGRAM_SIZE); 

	gbnpacket_t *curr_packet = allocate_packet (); 
	gbnpacket_t *curr_ack = allocate_packet (); 
	gbnpacket_t **packets; 

	if (argc != 3) { 
		fprintf (stderr,"Usage: %s PortNumber enableLoss\n\n", argv[0]);
		exit (1);
	}

	// Unpack arguments
	portno = atoi(argv[1]);
	session_lossrate (argv[2], LOSS_RATE, CORRUPT_RATE, &loss_rate, &corrupt_rate); 

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0)
		error("Error opening socket!");

	// Fill my_addr
	memset ((char *) &my_addr, 0, addr_len);
	my_addr.sin_family = AF_INET; 
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	my_addr.sin_port = htons(portno);	

	if (bind(sockfd, (struct sockaddr *)&my_addr, addr_len) < 0)
		error("Bind failed!");

	while (1) { 

		// Set sigaction handler for timer
		myAction.sa_handler = CatchAlarm;
		if (sigfillset (&myAction.sa_mask) < 0)  // Block everything in handler
			error ("sigfillset() failed");
			
		myAction.sa_flags = 0;
		if (sigaction (SIGALRM, &myAction, 0) < 0)
			error ("Error setting the alarm!"); 

		printf ("Waiting on port %d\n", portno);
		recvlen = recvfrom (sockfd, fname_buffer, FNAME_BUFSIZE, 0, (struct sockaddr *)&rem_addr, &addr_len); 
		
		if (recvlen > 0) {
			fname_buffer[recvlen] = '\0';
			printf ("Received file name: \"%s\"\n", fname_buffer);
		}
		

		// Process file into datagrams
		int flen;
		int fopen_err = 0;  
		
		fp = fopen (fname_buffer, "r"); 
		if (fp == NULL ) { // what if file not found ?
			gbnpacket_t *nofile = allocate_packet (); 
			nofile = make_packet (404, NULL, 404, 0);			
			send_packet (sockfd, (struct sockaddr *)&rem_addr, nofile);
			error("ERROR: file does not exists"); 
		} 

		// Get stats on datasize etc. 
		datasize = get_filelen (fp, &fopen_err);
		total_packets = (datasize + (PAYLOAD_SIZE - 1)) / PAYLOAD_SIZE;
		int last_seqno = PAYLOAD_SIZE * total_packets; 
		printf ("Datasize: %d, total_packets: %d, last_seqno: %d\n", datasize, total_packets, last_seqno);

		// Allocate space for total packets + 1 so we can include termination packet
		if ( (packets = malloc (sizeof (gbnpacket_t *) * (total_packets + 1))) == NULL)
			error ("Couldn't allocate space for list of packets!"); 
		memset (packets, 0, sizeof (gbnpacket_t *) * total_packets);

		for (i = 0; i < total_packets; i++) {
			packets[i] = filechunk_to_packet (fp, &seq_counter);
		}
		packets[total_packets] = make_packet (seq_counter, NULL, 1, 0);

		fclose (fp);

		struct timeval tv; // time count 
		gettimeofday(&tv,NULL);
		long int startTime; 
		startTime = tv.tv_sec;  
	
		/* ------ START GBN PROCEEDINGS ------- */
		sendflag = 1; 
		timeout = 0; 

		while (1) {

			// If we get a timeout, rebase nextseqnum and reset timeout flag to 0. 
			if ( timeout ) {
				if ( nextseqnum == last_seqno ) {
					send_packet (sockfd, (struct sockaddr *)&rem_addr, packets[total_packets]);
					alarm (TIMEOUT_SECS);		
				}			
				printf ("\tHaven't received ACK for %d\n", base);
				nextseqnum = base; 
				timeout = 0; 
			}

			// Only send a packet if sending is enabled, the sequence number is in the window, and
			// we haven't sent the last packet already 
			while ( sendflag && (nextseqnum < (base + WND_SIZE)) && (nextseqnum <= last_seqno) ) {
	
				// Check if sequence number is greater than last seqno; send terminating packet if yes
				if ( nextseqnum == last_seqno ) {
					alarm (TIMEOUT_SECS);
					send_packet (sockfd, (struct sockaddr *)&rem_addr, packets[total_packets]);
					break;
				}

				nextseqidx = seq_index (nextseqnum);

				gettimeofday(&tv,NULL);
				printf ("\n<--- SENDING PACKET %d. \n\tTIME ELAPSED %ld sec.\n", packets[nextseqidx]->seq_no, tv.tv_sec-startTime);
				// Set timeout only if this is the base packet, i.e., the oldest un-acked pkt
				if (base == nextseqnum) {
					alarm (TIMEOUT_SECS); 
				}

				// "Randomly" decide whether to drop this packet, otherwise send it
				if ( loss_rate > drand48() ) {
					printf ("x~x~x Uh oh, dropped packet %d in transit!\n", nextseqnum);
				} else {
					send_packet (sockfd, (struct sockaddr *)&rem_addr, packets[nextseqidx]);
				}

				nextseqnum += PAYLOAD_SIZE; 

				if (nextseqnum == (base + WND_SIZE)) {
					// done sending; no more packets until we get an ack for the base packet or timer runs out				
					sendflag = 0;  
				}
			}

			// Get packets from the receiver
			// recvlen = recvfrom (sockfd, recv_buffer, DGRAM_SIZE, 0, (struct sockaddr *) &rem_addr, &addr_len);
			while ((recvlen = recvfrom (sockfd, recv_buffer, DGRAM_SIZE, 0, (struct sockaddr *) &rem_addr, &addr_len)) < 0) {
			// any invalid ACK received will get stuck in this WHILE

				if (timeout == 1) /* Alarm went off */
				{		
						printf ("timed out\n");
						break; // exit this loop and repeat sending the packets inside cwnd
				}
			}

			if ( recvlen > 0 ) {
				
				// Packetize the received data and clear buffer immediately

				curr_ack = receive_packet (recv_buffer);
				gettimeofday(&tv,NULL);
				printf ("\n---> RECEIEVED ACK %d, type %d. \n\tTIME ELAPSED %ld sec.\n", curr_ack->seq_no, curr_ack->type, tv.tv_sec-startTime);				
				memset (recv_buffer, 0, DGRAM_SIZE);

				if ( corrupt_rate > drand48() ) {
				
					printf ("\n*~*~ Oh no, incoming ACK %d was corrupted! Discarding...\n", curr_ack->seq_no);
				
				} else {

					// Check for termination ACK, i.e., that receiver knows we are done
					if ( curr_ack->type == 1 ) {
						printf ("\nSent all packets, received termination ACK from sender; terminating!\n");
						dealloc_pkts (packets, total_packets);

						free (fname_buffer);
						free (recv_buffer);

						return(1); 					
					}

					// If we get a new, bigger ACK, then reset sendflag & alarm. 
					if ((last_ack_received > -1) && (curr_ack->seq_no > last_ack_received)) {
						sendflag = 1; 
						alarm (TIMEOUT_SECS);

						base = curr_ack->seq_no + PAYLOAD_SIZE; 
					}
					
					last_ack_received = curr_ack->seq_no;
				}
		 	}
		} 	
	}
}


void
dealloc_pkts (gbnpacket_t **packets, int total_packets)
{
	int i; 
	for (i = 0; i < total_packets + 1; i++)
		free (packets[i]);

	free (packets);	
}


int
get_filelen (FILE *file_descriptor, int *fopen_err)
{
	int flen; 

	// Set error flag if fopen fails, otherwise open and read file 
	if ( file_descriptor == NULL ) {
		*fopen_err = 1; 	
	} else {

		if ( fseek( file_descriptor, 0, SEEK_END ) != 0) {
			*fopen_err = 1; 
		} else {
			flen = ftell (file_descriptor);
			rewind (file_descriptor);
		}
	}

	return flen;
}


int 
seq_index (int seq_number)
{	
	int i = ( seq_number / PAYLOAD_SIZE );
	return i; 
}


gbnpacket_t *
filechunk_to_packet (FILE *file_descriptor, int *seq_counter)
{
	// Split file into chunks and put each into a datagram with a header
	int length; 
	char *data;
	gbnpacket_t *packet;

	if ( (data = malloc (PAYLOAD_SIZE)) == NULL )
		error ("Error allocating packet buffer!");
	memset (data, 0, PAYLOAD_SIZE);

	// Num bytes read from file will be the packet's length value
	length = fread (data, 1, PAYLOAD_SIZE, file_descriptor); 

	// Packets of type 2 are data packets
	packet = make_packet (*seq_counter, data, 2, length);

	*seq_counter += PAYLOAD_SIZE; 	
	free (data);
	return packet; 
}


void 
CatchAlarm (int ignored) 
{  
	// What to do when the alarm expires
	sendflag = 1;  
	timeout = 1;  // I.e., the timer has timed out, need to deal with it
	printf ("\n*** Timeout! Resetting sendflag. \n");
}


