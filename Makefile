CC=gcc
CFLAGS=-g -I.
DEPS = # header file 
REC = receiverGBN.o
SEND = senderGBN.o

all: senderGBN receiverGBN

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

receiverGBN: $(REC)
	$(CC) -o $@ $^ $(CFLAGS)

senderGBN: $(SEND)
	$(CC) -o $@ $^ $(CFLAGS)	

clean: 
	rm -rf *.o senderGBN receiverGBN


