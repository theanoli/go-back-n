// Arguments: sender_hostname sender_portnum filename enable_congestionControl

#include "headerGBN.h"

long int write_packet_to_file (gbnpacket_t *, char *, long int); 
void write_eof (char *, long int);


int 
main (int argc, char *argv[])
{
	int recvlen;  // size of received message
	int current_ack_no; 
	int expected_packet;  // next anticipated packet
	current_ack_no = expected_packet = 0; 
	int last_type;  // type of last received packet

	float loss_rate, corrupt_rate;  // fraction of packets to "lose"
	long int last_file_pos = 0; 

	int sockfd, portno, n;
	struct sockaddr_in rem_addr;
	socklen_t addr_len = sizeof(rem_addr); 
	struct hostent *server;
	char *fname, *recv_buffer; 

	void *seq_no_ptr;
	void *length_ptr; 
	void *type_ptr; 
	void *data_ptr;

	gbnpacket_t *curr_packet = allocate_packet (); 
	gbnpacket_t *curr_ack = allocate_packet (); 

	FILE *recv_fp; 

	// Check for correct number of arguments
	if (argc < 5) {
		fprintf(stderr, "ERROR! Not enough arguments.\n Usage: %s sender_hostname sender_portno filename MaxSegmentSize WindowSize enable_loss\n\n", argv[0]);		
		exit (0);
	}

	// Unpack arguments
	portno = atoi(argv[2]);
	fname = argv[3];
	session_lossrate (argv[2], LOSS_RATE, CORRUPT_RATE, &loss_rate, &corrupt_rate); 

	server = gethostbyname(argv[1]);
	if (!server)
		error ("Error obtaining address of server!");

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0)
		error ("Error opening socket!");

	// fill rem_addr
	memset ((char *) &rem_addr, 0, sizeof(rem_addr));
	rem_addr.sin_family = AF_INET; 
	memcpy ((char *)server->h_addr, (char *)&rem_addr.sin_addr.s_addr, server->h_length);
	rem_addr.sin_port = htons(portno);	

	// Send file request to sender
	n = sendto (sockfd, fname, strlen(fname), 0, (struct sockaddr *)&rem_addr, addr_len);
	if (n < 0)
		error("Error sending message to sender!");

	printf ("Sent filename, awaiting response...\n");

	// Create new filename using extension from requested file
	char *file_ext = strchr(fname, '.'); 
	
	char new_fname[100] = "newfile"; 
	if ( file_ext != NULL )
		strcat (new_fname, file_ext);

	if ( (recv_fp = fopen (new_fname, "w")) == NULL )
		error ("Error opening write file!");
	fclose (recv_fp);

	// Allocate buffer where we will put incoming data
	if ( (recv_buffer = malloc (DGRAM_SIZE)) == NULL) 
		error ("Error allocating receive buffer!");
	memset (recv_buffer, 0, DGRAM_SIZE);

	struct timeval tv;
	gettimeofday(&tv,NULL);
	long int startTime; 
	startTime = tv.tv_sec; 
	
	while(1) {

		// Receive GBN packet from sender 
		recvlen = recvfrom (sockfd, recv_buffer, DGRAM_SIZE, 0, (struct sockaddr *) &rem_addr, &addr_len);
		
		if ( recvlen > 0 ) {
			curr_packet = receive_packet (recv_buffer); 

			if ( curr_packet->type == 404 ) {
				printf ("File not found.\n");
				exit(0) ;
			}

			// take down the time of the day. 
			gettimeofday(&tv,NULL);			
			printf ("\n---> RECEIVED PACKET %d from sender, EXPECTED %d. \n\tTIME ELAPSED %ld sec.\n", curr_packet->seq_no, expected_packet,tv.tv_sec-startTime);

			if ( corrupt_rate > drand48() ) {

				printf ("\n*~*~ Oh no, incoming packet %d was corrupted! Discarding...\n", curr_packet->seq_no);

			} else {

				if ( curr_packet->seq_no == expected_packet ) {

					// This is the packet we wanted next; check for termination signal
					if ( curr_packet->type == 1 ) {
						gettimeofday(&tv,NULL);
						printf ("\nReceived termination signal; sent termination ACK! \n\tTIME ELAPSED %ld sec.\n", tv.tv_sec-startTime);
						curr_ack = make_packet (current_ack_no, NULL, 1, 0);
						send_packet (sockfd, (struct sockaddr *)&rem_addr, curr_ack);
			
						free (recv_buffer);
						free (curr_packet);
						free (curr_ack);

						close (sockfd);

						return(1);
					}

					// Increment expected_packet so we receive a new one in next round, but save previous value
					current_ack_no = expected_packet; 
					expected_packet += PAYLOAD_SIZE; 

					// Write payload to file
					last_file_pos = write_packet_to_file (curr_packet, new_fname, last_file_pos);
				}
			}	

			// clear curr_packet to prepare for next one
			memset (curr_packet, 0, sizeof (gbnpacket_t));
			memset (recv_buffer, 0, DGRAM_SIZE);

			curr_ack = make_packet (current_ack_no, NULL, 2, 0);
			
			if ( loss_rate > drand48() ) { // compare to a number between 0 and 1 
				// drop packet and go back to waiting for packets
				printf("\nx~x~x Uh oh, dropped an ACK! %d \n", curr_ack->seq_no); 
			} else {
				send_packet (sockfd, (struct sockaddr *)&rem_addr, curr_ack);
				// take down the time of the day. 
				gettimeofday(&tv,NULL);
				printf ("\n<--- SENDING ACK %d. \n\tTIME ELAPSED %ld sec.\n", current_ack_no, tv.tv_sec-startTime);
			}	
		}
	}
}


long int
write_packet_to_file (gbnpacket_t *packet, char *filename, long int last_file_position 
)
{
	FILE *fp;
	int n; 

	if ( (fp = fopen (filename, "r+")) == NULL )
		error ("Error opening write file!");

	fseek (fp, last_file_position, SEEK_SET);

	n = fwrite (packet->data, 1, (size_t) packet->length, fp);
	if ( n != packet->length )
		error ("Error writing to file!");

	last_file_position = ftell (fp);

	if ( fclose (fp) == EOF )
		error ("Error closing file!"); 

	return last_file_position;
}

